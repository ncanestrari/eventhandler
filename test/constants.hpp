/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         define a simple test for the event handler library
 */

#ifndef CONSTANT_HPP
#define CONSTANT_HPP

namespace eventhandler {
class Constant {
private:
  Constant() = default;
public:
  const double data_d[3] = {1.0, 1.2, 1.4};
  const char * data_s1   = "pippo";
  const int    data_i    = 1;
  const float  data_f    = 1.;
  const char * data_s2   = "pippa";
  const char * data_s3   = "pippi";
  static Constant & instance() {
    static Constant item;
    return item;
  }
};
};

#endif /* CONSTANT_HPP */
