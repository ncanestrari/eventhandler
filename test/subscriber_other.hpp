/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         define a simple test for the event handler library
 */

#pragma once

#include "subscriber.hpp"
#include "event_other.hpp"

using namespace eventhandler;

class Subscriber1 : public Subscriber {
DELETE_COPY_AND_MOVE_CTOR(Subscriber1)
public:
  Subscriber1() : Subscriber({Event1::TOPIC(), Event2::TOPIC()}) {}
  void update(const Event &ev) override;
};

class Subscriber2 : public Subscriber {
DELETE_COPY_AND_MOVE_CTOR(Subscriber2)
public:
  Subscriber2() : Subscriber({Event3::TOPIC(), Event4::TOPIC()}) {}
  void update(const Event &ev) override;
};

class Subscriber3 : public Subscriber {
DELETE_COPY_AND_MOVE_CTOR(Subscriber3)
public:
  Subscriber3() : Subscriber({Event5::TOPIC(), Event6::TOPIC()}) {}
  void update(const Event &ev) override;
};

class Subscriber4 : public Subscriber {
DELETE_COPY_AND_MOVE_CTOR(Subscriber4)
public:
  Subscriber4()
      : Subscriber({Event1::TOPIC(), Event2::TOPIC(),
                    Event5::TOPIC(), Event6::TOPIC()}) {}
  void update(const Event &ev) override;
};

