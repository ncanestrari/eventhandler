/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         define a simple test for the event handler library
 */

#include "event_other.hpp"
#include "publisher.hpp"
#include "subscriber_other.hpp"
#include "publisher_other.hpp"

#include <gtest/gtest.h>


TEST(EventHandler, Normal) {
  Subscriber1 s1;
  Subscriber2 s2;
  Subscriber3 s3;
  Subscriber4 s4;
  User1 p1;
  p1.go();
}
