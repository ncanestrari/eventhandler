/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         define a simple test for the event handler library
 */

#pragma once

#include "publisher.hpp"
#include "event_other.hpp"
#include "macros.hpp"

#include <chrono>
#include <thread>

using namespace eventhandler;

class User1 : public Publisher {
DELETE_COPY_AND_MOVE_CTOR(User1)
public:
  User1() = default;
  void go();
};
