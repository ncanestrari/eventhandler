/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         define a simple test for the event handler library
 */

#include "subscriber_other.hpp"
#include "event_other.hpp"
#include "constants.hpp"

#include <gtest/gtest.h>

using namespace eventhandler;

void Subscriber1::update(const Event &ev) {
  if (ev.topic() == Event1::TOPIC()) {
    double d[3];
    ev.what(d);
    ASSERT_EQ(d[0], Constant::instance().data_d[0]);
    ASSERT_EQ(d[1], Constant::instance().data_d[1]);
    ASSERT_EQ(d[2], Constant::instance().data_d[2]);
  } else if (ev.topic() == Event2::TOPIC()) {
    char label[32];
    ev.what(label);
    ASSERT_STREQ(label, Constant::instance().data_s1);
  }
}

void Subscriber2::update(const Event &ev) {
  if (ev.topic() == Event3::TOPIC()) {
    int d;
    ev.what(&d);
    ASSERT_EQ(d, Constant::instance().data_i);
  } else if (ev.topic() == Event4::TOPIC()) {
    float f;
    ev.what(&f);
    ASSERT_EQ(f, Constant::instance().data_f);
  }
}

void Subscriber3::update(const Event &ev) {
  if (ev.topic() == Event5::TOPIC()) {
    char label[32];
    ev.what(label);
    ASSERT_STREQ(label, Constant::instance().data_s2);
  } else if (ev.topic() == Event6::TOPIC()) {
    char label[32];
    ev.what(label);
    ASSERT_STREQ(label, Constant::instance().data_s3);
  }
}

void Subscriber4::update(const Event &ev) {
  if (ev.topic() == Event1::TOPIC()) {
    double d[3];
    ev.what(d);
    ASSERT_EQ(d[0], Constant::instance().data_d[0]);
    ASSERT_EQ(d[1], Constant::instance().data_d[1]);
    ASSERT_EQ(d[2], Constant::instance().data_d[2]);
  } else if (ev.topic() == Event2::TOPIC()) {
    char label[32];
    ev.what(label);
    ASSERT_STREQ(label, Constant::instance().data_s1);
  } else if (ev.topic() == Event5::TOPIC()) {
    char label[32];
    ev.what(label);
    ASSERT_STREQ(label, Constant::instance().data_s2);
  } else if (ev.topic() == Event6::TOPIC()) {
    char label[32];
    ev.what(label);
    ASSERT_STREQ(label, Constant::instance().data_s3);
  }
}
