/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         define a simple test for the event handler library
 */

#pragma once

#include "event.hpp"

#include <cstring>

using namespace eventhandler;

class Event1 : public Event {
UNIQUE_ID
DELETE_COPY_AND_MOVE_CTOR(Event1)
private:
  double m_d[3];

public:
  Event1(const double d1, const double d2, const double d3) {
    m_d[0] = d1;
    m_d[1] = d2;
    m_d[2] = d3;
  }
  void what(void *t) const override { memcpy(t, m_d, sizeof(m_d)); }
};

class Event2 : public Event {
UNIQUE_ID
DELETE_COPY_AND_MOVE_CTOR(Event2)
private:
  char m_label[32];

public:
  explicit Event2(const char *l) { strncpy(m_label, l, 32); }
  void what(void *t) const override { memcpy(t, m_label, 32); }
};

class Event3 : public Event {
UNIQUE_ID
DELETE_COPY_AND_MOVE_CTOR(Event3)
private:
  int m_d;

public:
  explicit Event3(const int d) : m_d(d) {}
  void what(void *t) const override { memcpy(t, &m_d, sizeof(int)); }
};

class Event4 : public Event {
UNIQUE_ID
DELETE_COPY_AND_MOVE_CTOR(Event4)
private:
  float m_d;

public:
  explicit Event4(const float d) : m_d(d) {}
  void what(void *t) const override { memcpy(t, &m_d, sizeof(float)); }
};

class Event5 : public Event {
UNIQUE_ID
DELETE_COPY_AND_MOVE_CTOR(Event5)
private:
  char m_label[32];

public:
  explicit Event5(const char *l) { strncpy(m_label, l, 32); }
  void what(void *t) const override { memcpy(t, m_label, 32); }
};

class Event6 : public Event {
UNIQUE_ID
DELETE_COPY_AND_MOVE_CTOR(Event6)
private:
  char m_label[32];

public:
  explicit Event6(const char *l) { strncpy(m_label, l, 32); }
  void what(void *t) const override { memcpy(t, m_label, 32); }
};

