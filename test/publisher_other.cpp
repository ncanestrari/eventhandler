/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         define a simple test for the event handler library
 */

#include "publisher_other.hpp"
#include "constants.hpp"

using namespace eventhandler;

void User1::go() {
  int i = 0;
  while (true) {
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    switch (i) {
    case 0:
      notify(Event1(Constant::instance().data_d[0], 
                    Constant::instance().data_d[1], 
                    Constant::instance().data_d[2]));
      break;
    case 1:
      notify(Event2(Constant::instance().data_s1));
      break;
    case 2:
      notify(Event3(Constant::instance().data_i));
      notify(Event3(Constant::instance().data_i));
      break;
    case 3:
      notify(Event4(Constant::instance().data_f));
      break;
    case 4:
      notify(Event5(Constant::instance().data_s2));
      notify(Event5(Constant::instance().data_s2));
      break;
    case 5:
      notify(Event6(Constant::instance().data_s3));
      break;
    default:
      goto EndLoop;
      break;
    }
    ++i;
  }
EndLoop:
  return;
}
