/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         declare virtual publisher class
 */

#pragma once

#include "macros.hpp"

namespace eventhandler {
class Broadcaster;
class Event;

/**
 * @brief       abstract class Publisher
 * @details     Users are welcome to derive their own classes from this
 */
class Publisher {
DELETE_COPY_AND_MOVE_CTOR(Publisher)
public:

  /**
   * @brief         default c'tor
   */
  Publisher() = default;

  /**
   * @brief         default d'tor
   */
  virtual ~Publisher(){};

  /**
   * @brief         notify the Broadcaster of the Event
   * @param[in]     ev, Event
   */
  void notify(const Event &ev);
};

}; // namespace eventhandler
