/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         declare the list of possible events as derived of the Event
 */

#pragma once

#define CRC32_MASK 0xEDB88320

namespace eventhandler {
class CRC {
public:
  static unsigned int crc32(const char *str) {
    int          i    = 0;
    unsigned int byte = 0;
    unsigned int crc  = 0xFFFFFFFF;
    unsigned int mask = 0;

    while (str[i] != 0) {
      byte = str[i];
      crc = crc ^ byte;
      for (int j = 7; j >= 0; --j) {
        mask = -(crc & 1);
        crc = (crc >> 1) ^ (CRC32_MASK & mask);
      }
      i += 1;
    }
    return ~crc;
  }
};
};
