/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         declare the virtual broadcaster class
 */

#pragma once

#include "event_topic.hpp"

#include <map>

namespace eventhandler {
class Event;
class Subscriber;

/**
 * @brief       class Broadcaster takes care of delivering events to the subscribers
 * @details     the class is singleton and it is opaque to the end user of the library
 */
class Broadcaster {
private:
  using mmts = std::multimap<EventTopic, Subscriber *>;
  mmts m_v;

  /**
   * @brief         default c'tor
   * @details       private for singleton design pattern requirements
   */
  Broadcaster() = default;

public:

  /**
   * @brief         get unique object of the class
   * @details       static qualifier ensure the function can be called without the object
   * @return        reference to the unique object
   */
  static Broadcaster &get() {
    static Broadcaster instance;
    return instance;
  }

  /**
   * @brief         update the subscription database
   * @param[in]     s, Subscriber pointer
   * @param[in]     topic, TOPIC to be associated
   */
  void addSubscriber(Subscriber * s, EventTopic topic);

  /**
   * @brief         notify the subscribers associated to the relevan TOPIC of the Event
   * @param[in]     ev, Event
   */
  void notify(const Event &e);
};

}; // namespace eventhandler
