/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         defines macros
 */

#pragma once


#define DELETE_COPY_AND_MOVE_CTOR(klass)                \
public:                                                 \
  klass(const klass & other) = delete;                  \
  klass(klass && other) = delete;                       \
  klass & operator = (const klass & other) = delete;    \
  klass & operator = (klass && other) = delete;

