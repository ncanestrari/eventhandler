/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         declare the list of possible events as derived of the Event
 */

#pragma once

namespace eventhandler {
using EventTopic = unsigned int;
};
