/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         declare the virtual subscriber class
 */

#pragma once

#include <vector>
#include "event.hpp"

namespace eventhandler {

using vtopic = std::vector<EventTopic>;
class Event;

/**
 * @brief       abstract class Subscriber
 * @details     Users are welcome to derive their own classes from this
 */
class Subscriber {
DELETE_COPY_AND_MOVE_CTOR(Subscriber)
protected:

  /**
   * @brief         subscribe to the given list of topics
   * @param[in]     v, vector of TOPIC
   */
  void subscribe(const vtopic &v);

public:

  /**
   * @brief         default c'tor
   */
  Subscriber() = default;

  /**
   * @brief         explicit c'tor
   * @param[in]     v, vector of TOPIC
   */
  explicit Subscriber(const vtopic &v) { subscribe(v); }


  virtual ~Subscriber(){};

  /**
   * @brief         update according to the Event
   * @param[in]     ev, Event
   */
  virtual void update(const Event &ev) = 0;
};

}; // namespace eventhandler
