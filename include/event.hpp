/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         declare the list of possible events as derived of the Event
 */

#pragma once

#include "event_topic.hpp"
#include "macros.hpp"
#include "crc.hpp"

#include <cctype>

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define AT __FILE__ TOSTRING(__LINE__)


namespace eventhandler {
using std::size_t;

/*
template<int N>
class CRC32 {
  unsigned int crc = 0xFFFFFFFF;
public:
  static constexpr unsigned int crc32(const char * str) {
    unsigned int ret = CRC32<N-1>::crc32(str + 1);
    return ret;
  }
};
*/


/**
 * @brief       macro infrastructure provides the UID to classes
 * @details     to be appended at the start of the class declaration
 */
#define UNIQUE_ID                                                 \
public:                                                           \
  /**                                                             \
   * @brief       static function to get the UID of the class     \
   *              can be called without the instance              \
   * @return      the topic                                       \
   */                                                             \
  static EventTopic TOPIC() {                                     \
    static EventTopic crc = CRC::crc32(AT);                       \
    return crc;                                                   \
  }                                                               \
  /**                                                             \
   * @brief       virtual function to get the UID of the class    \
   *              can be called from inherited object             \
   * @return      the topic                                       \
   */                                                             \
  virtual EventTopic topic() const override { return TOPIC(); }

/**
 * @brief       abstract class Event
 * @details     Users are welcome to derive their own classes from this
 */
class Event {
public:
  /**
   * @brief       static function to get the UID of the class
   *              can be called without the instance
   * @return      the topic
   */
  static EventTopic TOPIC() {
    static EventTopic crc = CRC::crc32(AT);
    return crc;
  }
    
  /**
   * @brief       virtual function to get the UID of the class
   *              can be called from inherited object
   * @return      the topic
   */
  virtual EventTopic topic() const { return TOPIC(); }

  /**
   * @brief         default c'tor
   */
  Event() = default;

  DELETE_COPY_AND_MOVE_CTOR(Event)

  /**
   * @brief         event can be emitted with a message, the receiver knows its size
   * @param[in,out] t, void pointer where the content of the message will be copied
   */
  virtual void what(void *t) const {};
};

}; // namespace eventhandler
