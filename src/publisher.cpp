/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         define the virtual publisher class
 */


#include "broadcaster.hpp"
#include "event.hpp"
#include "publisher.hpp"


namespace eventhandler {

void Publisher::notify(const Event &ev) { Broadcaster::get().notify(ev); }

}; // namespace eventhandler
