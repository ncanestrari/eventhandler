/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         define the virtual subscriber class
 */

#include "subscriber.hpp"
#include "broadcaster.hpp"
#include "event.hpp"


namespace eventhandler {

void Subscriber::subscribe(const std::vector<EventTopic> &v) {
  for (auto it : v) {
    Broadcaster::get().addSubscriber(this, it);
  }
}

}; // namespace eventhandler
