/**
 * @file
 * @author        Niccolo Canestrari
 * @copyright     Copyright © 2019 Solystic SA. All right reserved
 * @brief         define the virtual broadcaster class
 */

#include "broadcaster.hpp"
#include "event.hpp"
#include "subscriber.hpp"

namespace eventhandler {
using mmts = std::multimap<EventTopic, Subscriber *>;
using mmtsi = mmts::iterator;

void Broadcaster::addSubscriber(Subscriber *s, EventTopic topic) {
  m_v.insert(std::make_pair(topic, s));
}

void Broadcaster::notify(const Event &e) {
  EventTopic evt = e.topic();
  for (mmtsi it = m_v.equal_range(evt).first;
       it != m_v.equal_range(evt).second; ++it) {
    it->second->update(e);
  }
}

}; // namespace eventhandler
